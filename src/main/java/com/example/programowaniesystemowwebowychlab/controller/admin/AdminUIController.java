package com.example.programowaniesystemowwebowychlab.controller.admin;

import com.example.programowaniesystemowwebowychlab.model.Product;
import com.example.programowaniesystemowwebowychlab.service.CategoryService;
import com.example.programowaniesystemowwebowychlab.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminUIController {
    private final CategoryService categoryService;
    private final ProductService productService;

    @GetMapping("/product-management")
    public String productManagement(Model model) {
        model.addAttribute("products", productService.getAll());
        return "admin/product-management";
    }

    @GetMapping("/product/{id}")
    public String viewProduct(@PathVariable Long id, Model model) {
        model.addAttribute("product", productService.get(id).orElseThrow());
        return "admin/product";
    }

    @GetMapping("/product/create")
    public String createForm(Model model) {
        model.addAttribute("product", new Product());
        model.addAttribute("categories", categoryService.getAll());
        return "admin/add-product-form";
    }

    @GetMapping("/product/{id}/update")
    public String updateForm(@PathVariable Long id, Model model) {
        model.addAttribute("product", productService.get(id).orElseThrow());
        model.addAttribute("categories", categoryService.getAll());
        return "admin/edit-product-form";
    }
}
