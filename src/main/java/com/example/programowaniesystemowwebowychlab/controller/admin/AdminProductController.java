package com.example.programowaniesystemowwebowychlab.controller.admin;

import com.example.programowaniesystemowwebowychlab.model.Product;
import com.example.programowaniesystemowwebowychlab.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
@RequestMapping("/admin/product")
public class AdminProductController {
    private final ProductService productService;

    @PostMapping("/create")
    public String create(@ModelAttribute Product product) {
        productService.save(product);
        return "redirect:/admin/product-management";
    }

    @PostMapping("/{id}/update")
    public String update(@ModelAttribute Product product, @PathVariable Long id) {
        productService.update(id, product);
        return "redirect:/admin/product-management";
    }

    @GetMapping("/{id}/delete")
    public String delete(@PathVariable Long id) {
        productService.delete(id);
        return "redirect:/admin/product-management";
    }
}
