package com.example.programowaniesystemowwebowychlab.controller.user;

import com.example.programowaniesystemowwebowychlab.model.User;
import com.example.programowaniesystemowwebowychlab.service.BasketService;
import com.example.programowaniesystemowwebowychlab.service.ProductService;
import com.example.programowaniesystemowwebowychlab.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    private final BasketService basketService;
    private final UserService userService;

    @GetMapping("/product/{id}/add-to-basket")
    public String addToBasket(@PathVariable Long id, Principal principal) {
        var user = (User) userService.loadUserByUsername(principal.getName());
        var product = productService.get(id).orElseThrow();
        basketService.addToBasket(user.getBasket().getId(), product);
        return "redirect:/shop";
    }

    @GetMapping("/product/{id}/remove-from-basket")
    public String removeFromBasket(@PathVariable Long id, Principal principal) {
        var user = (User) userService.loadUserByUsername(principal.getName());
        var product = productService.get(id).orElseThrow();
        basketService.removeFromBasket(user.getBasket().getId(), product);
        return "redirect:/basket-summary";
    }

    @GetMapping("/product/{id}/increment-in-basket")
    public String incrementQuantityInBasket(@PathVariable Long id, Principal principal) {
        var user = (User) userService.loadUserByUsername(principal.getName());
        var product = productService.get(id).orElseThrow();
        basketService.incrementQuantityInBasket(user.getBasket().getId(), product);
        return "redirect:/basket-summary";
    }

    @GetMapping("/product/{id}/decrement-in-basket")
    public String decrementQuantityInBasket(@PathVariable Long id, Principal principal) {
        var user = (User) userService.loadUserByUsername(principal.getName());
        var product = productService.get(id).orElseThrow();
        basketService.decrementQuantityInBasket(user.getBasket().getId(), product);
        return "redirect:/basket-summary";
    }

    @GetMapping("/submit-order")
    public String submitOrder(Principal principal) {
        var user = (User) userService.loadUserByUsername(principal.getName());
        basketService.emptyBasket(user.getBasket().getId());
        return "user/order-submitted";
    }

}
