package com.example.programowaniesystemowwebowychlab.controller.user;

import com.example.programowaniesystemowwebowychlab.model.User;
import com.example.programowaniesystemowwebowychlab.service.BasketService;
import com.example.programowaniesystemowwebowychlab.service.CategoryService;
import com.example.programowaniesystemowwebowychlab.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class UIController {
    private final BasketService basketService;
    private final CategoryService categoryService;
    private final UserService userService;

    @GetMapping("/shop")
    public String shop(Model model, Principal principal) {
        var user = (User) userService.loadUserByUsername(principal.getName());
        model.addAttribute("numberOfBasketItems", user.getBasket().getNumberOfItems());
        model.addAttribute("categories", categoryService.getAll());
        return "user/shop";
    }

    @GetMapping("/basket-summary")
    public String basketSummary(Model model, Principal principal) {
        var user = (User) userService.loadUserByUsername(principal.getName());
        model.addAttribute("items", user.getBasket().getItems());
        model.addAttribute("numberOfItems", user.getBasket().getNumberOfItems());
        model.addAttribute("total", user.getBasket().getTotal());
        return "user/basket-summary";
    }
}
