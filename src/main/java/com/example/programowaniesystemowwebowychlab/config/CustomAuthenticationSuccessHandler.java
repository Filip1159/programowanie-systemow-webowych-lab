package com.example.programowaniesystemowwebowychlab.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.example.programowaniesystemowwebowychlab.model.Role.ADMIN;
import static com.example.programowaniesystemowwebowychlab.model.Role.USER;

@Configuration
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authResult) throws IOException {
        var roles =  authResult.getAuthorities();
        if (roles.contains(ADMIN)){
            response.sendRedirect("/admin/product-management");
        } else if (roles.contains(USER)) {
            response.sendRedirect("/shop");
        }
    }
}
