package com.example.programowaniesystemowwebowychlab.service;

import com.example.programowaniesystemowwebowychlab.model.BasketItem;
import com.example.programowaniesystemowwebowychlab.model.Product;
import com.example.programowaniesystemowwebowychlab.repo.BasketItemRepo;
import com.example.programowaniesystemowwebowychlab.repo.BasketRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class BasketService {
    private final BasketRepo repo;
    private final BasketItemRepo basketItemRepo;

    public void addToBasket(Long basketId, Product product) {
        var itemForProduct = findForProduct(basketId, product);
        if (itemForProduct.isPresent()) {
            itemForProduct.get().incrementQuantity();
            basketItemRepo.save(itemForProduct.get());
        } else {
            var basket = repo.findById(basketId).orElseThrow();
            var newItem = BasketItem.builder()
                    .basket(basket)
                    .product(product)
                    .quantity(1)
                    .build();
            basketItemRepo.save(newItem);
        }
    }

    public void emptyBasket(Long basketId) {
        var basket = repo.findById(basketId).orElseThrow();
        basketItemRepo.deleteAll(basket.getItems());
    }

    public void removeFromBasket(Long basketId, Product product) {
        var itemForProduct = findForProduct(basketId, product).orElseThrow();
        basketItemRepo.delete(itemForProduct);
    }

    public void incrementQuantityInBasket(Long basketId, Product product) {
        var itemForProduct = findForProduct(basketId, product).orElseThrow();
        itemForProduct.incrementQuantity();
        basketItemRepo.save(itemForProduct);
    }

    public void decrementQuantityInBasket(Long basketId, Product product) {
        var itemForProduct = findForProduct(basketId, product).orElseThrow();
        itemForProduct.decrementQuantity();
        if (itemForProduct.getQuantity() <= 0) {
            basketItemRepo.delete(itemForProduct);
        } else {
            basketItemRepo.save(itemForProduct);
        }
    }

    private Optional<BasketItem> findForProduct(Long basketId, Product product) {
        var basket = repo.findById(basketId).orElseThrow();
        return basket.getItems().stream()
                .filter(item -> item.getProduct().equals(product))
                .findFirst();
    }
}
