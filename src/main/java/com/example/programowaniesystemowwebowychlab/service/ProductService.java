package com.example.programowaniesystemowwebowychlab.service;

import com.example.programowaniesystemowwebowychlab.model.Product;
import com.example.programowaniesystemowwebowychlab.repo.ProductRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class ProductService {
    private final ProductRepo repo;
    private final CategoryService categoryService;

    public List<Product> getAll() {
        return repo.findAll();
    }

    public Optional<Product> get(Long id) {
        return repo.findById(id);
    }

    public Product save(Product product) {
        var newProduct = Product.builder()
                .name(product.getName())
                .price(product.getPrice())
                .weight(product.getWeight())
                .description(product.getDescription())
                .category(categoryService.get(product.getCategory().getId()))
                .build();
        repo.save(newProduct);
        return newProduct;
    }

    public Product update(Long id, Product product) {
        var existingProduct = repo.findById(id).orElseThrow();
        existingProduct.setName(product.getName());
        existingProduct.setPrice(product.getPrice());
        existingProduct.setWeight(product.getWeight());
        existingProduct.setDescription(product.getDescription());
        existingProduct.setCategory(categoryService.get(product.getCategory().getId()));
        repo.save(existingProduct);
        return existingProduct;
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }
}
