package com.example.programowaniesystemowwebowychlab.service;

import com.example.programowaniesystemowwebowychlab.model.Category;
import com.example.programowaniesystemowwebowychlab.repo.CategoryRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepo repo;

    public List<Category> getAll() {
        return repo.findAll();
    }

    public Category get(Long id) {
        return repo.findById(id).orElseThrow();
    }
}
