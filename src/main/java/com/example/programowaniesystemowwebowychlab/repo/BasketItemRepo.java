package com.example.programowaniesystemowwebowychlab.repo;

import com.example.programowaniesystemowwebowychlab.model.BasketItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BasketItemRepo extends JpaRepository<BasketItem, Long> {
}
