package com.example.programowaniesystemowwebowychlab.repo;

import com.example.programowaniesystemowwebowychlab.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Long> {
}
