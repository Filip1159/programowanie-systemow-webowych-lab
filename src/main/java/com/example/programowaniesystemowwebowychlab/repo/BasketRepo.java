package com.example.programowaniesystemowwebowychlab.repo;

import com.example.programowaniesystemowwebowychlab.model.Basket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BasketRepo extends JpaRepository<Basket, Long> {
}
