package com.example.programowaniesystemowwebowychlab.repo;

import com.example.programowaniesystemowwebowychlab.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepo extends JpaRepository<Product, Long> {
}
