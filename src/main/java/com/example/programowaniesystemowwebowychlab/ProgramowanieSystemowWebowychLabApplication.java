package com.example.programowaniesystemowwebowychlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgramowanieSystemowWebowychLabApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProgramowanieSystemowWebowychLabApplication.class, args);
    }

}
