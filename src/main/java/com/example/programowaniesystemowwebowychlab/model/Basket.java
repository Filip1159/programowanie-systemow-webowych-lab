package com.example.programowaniesystemowwebowychlab.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Basket {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "basket")
    @ToString.Exclude
    private List<BasketItem> items;

    @OneToOne(mappedBy = "basket", fetch = LAZY)
    @ToString.Exclude
    private User user;

    public int getNumberOfItems() {
        return items.stream()
                .reduce(0,
                        (total, item) -> total + item.getQuantity(),
                        Integer::sum
                );
    }

    public double getTotal() {
        return items.stream()
                .reduce(0.0,
                        (total, item) -> total + item.getProduct().getPrice() * item.getQuantity(),
                        Double::sum
                );
    }
}
