insert into category (name) values ('Żywność'), ('Chemia'), ('Alkohole');

insert into product (name, price, weight, description, category_id)
values ('Chleb', '3.99', '0.6', 'Chleb żytni krojony z ziarnami słonecznika, Piekarnia Hert', 1),
       ('Masło', '7.59', '0.5', 'Masło Piątnica, zawartość tłuszczu 85%, z polskiego mleka', 1),
       ('Woda', '1.69', '1.5', 'Woda niegazowana Ustronianka, oficjalna woda polskiej reprezentacji w piłce nożnej', 1),
       ('Domestos', '8.95', '0.75', 'Środek do czyszczenia toalet Domestos Zero kamienia, zabija bakterie i grzyby', 2),
       ('Pasta do zębów', '10.99', '0.15', 'Pasta wybielająca Colgate z węglem aktywnym dla pięknego i zdrowego uśmiechu', 2),
       ('Płyn do naczyń', '6.89', '0.75', 'Płyn do naczyń Fairy, dwa razy więcej czystych talerzy', 2),
       ('Metaxa', '56.95', '0.7', 'Brandy Metaxa to tworzony od 120 lat, najbardziej znany na świecie grecki trunek', 3),
       ("Ballantine's", '82.99', '1.0', 'Ballantine''s Finest to whisky typu Blended o złożonym i eleganckim smaku, leżakująca minimum 3 lata', 3),
       ('Amarena', '6.89', '1.0', 'Napój bogów. Jeden z składników eliksiru Panoramixa. Sprzedawany w sieci Biedronka', 3);

insert into basket (id) values (1), (2);

insert into user (username, name, surname, password, role, basket_id)
values  ('janek007', 'Jan', 'Kowalski', '{bcrypt}$2y$12$yJKwJH8SWLBGSvPiggXQdelhMuZNhbQI3WIrNltRN1cE9JcNoNRVi', 'USER', 1),  -- qwerty
        ('ewa-nowak', 'Ewa', 'Nowak', '{bcrypt}$2y$12$s8GQ5Prs915abrGV/WxCquQuas1xvnqaDZ2iQrLzAKp2TthdfEBgK', 'ADMIN', 2);  -- abcd
